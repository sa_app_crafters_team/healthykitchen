package com.shamoon.healthykitchen

import com.shamoon.healthykitchen.controller.service.Api
import org.junit.Assert
import org.junit.Test

class ApiUnitTest {
    @Test
    fun `canGetCategories` () {
        // call the api
        val api = Api().getApi()
        val response = api!!.getRecipeCategories()!!.execute()

        Assert.assertEquals(response.code(), 200)
    }

    @Test
    fun `canGetRecipes` () {
        // call the api
        val api = Api().getApi()
        val response = api!!.search("Beef")!!.execute()

        Assert.assertEquals(response.code(), 200)
    }
}