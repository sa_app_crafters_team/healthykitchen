package com.shamoon.healthykitchen.controller.service

import com.shamoon.healthykitchen.model.api.RecipeCategoriesResponse
import com.shamoon.healthykitchen.model.api.RecipesResponse

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {
    @GET("categories.php")
    fun getRecipeCategories(): Call<RecipeCategoriesResponse?>?

    @GET("search.php")
    fun search(@Query("s") sort: String): Call<RecipesResponse?>?
}