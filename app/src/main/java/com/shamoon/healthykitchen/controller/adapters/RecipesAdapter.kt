package com.shamoon.healthykitchen.controller.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import com.shamoon.healthykitchen.BR
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.shamoon.healthykitchen.model.api.Meal
import com.shamoon.healthykitchen.views.recipies.RecipesViewModel

class RecipesAdapter : RecyclerView.Adapter<RecipesAdapter.GenericViewHolder> {
    private var layoutId: Int
    private  var recipes: ArrayList<Meal> = mutableListOf<Meal>() as ArrayList<Meal>
    private  var viewModel: RecipesViewModel

    constructor(
        @LayoutRes layoutId: Int,
        viewModel: RecipesViewModel?
    ) {
        this.layoutId = layoutId
        this.viewModel = viewModel!!
    }

    private fun getLayoutIdForPosition(position: Int): Int {
        return layoutId
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericViewHolder {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            viewType,
            parent,
            false
        )

        return GenericViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return recipes?.size ?: 0
    }

    override fun onBindViewHolder(holder: GenericViewHolder, position: Int) {
        holder.bind(viewModel, position)
    }

    override fun getItemViewType(position: Int): Int {
        return getLayoutIdForPosition(position)
    }

    fun setRecipes(recipes: ArrayList<Meal>) {
        this.recipes.clear()
        this.recipes.addAll(recipes)
    }


    class GenericViewHolder : RecyclerView.ViewHolder {
        var binding: ViewDataBinding

        constructor(binding: ViewDataBinding) : super(binding.root) {
            this.binding = binding
        }

        fun bind(viewModel: RecipesViewModel?, position: Int?) {
            binding.setVariable(BR.position, position)
            binding.setVariable(BR.viewModelRecipes, viewModel)
            binding.executePendingBindings()
        }

    }


}