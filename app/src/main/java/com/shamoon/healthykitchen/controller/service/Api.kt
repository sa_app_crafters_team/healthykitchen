package com.shamoon.healthykitchen.controller.service

import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory


class Api {
    private var api: ApiInterface? = null
    private val BASE_URL = "https://www.themealdb.com/api/json/v1/1/"

    public fun getApi(): ApiInterface? {
        if (api == null) {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .build()
            api = retrofit.create(ApiInterface::class.java)
        }
        return api
    }
}