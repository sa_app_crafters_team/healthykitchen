package com.shamoon.healthykitchen.controller.bindings

import android.widget.ImageView
import android.widget.VideoView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.shamoon.healthykitchen.R
import com.shamoon.healthykitchen.model.api.Meal


object CustomViewBindings {
    @BindingAdapter("setAdapter")
    @JvmStatic
    fun bindRecyclerViewAdapter(
        recyclerView: RecyclerView,
        adapter: RecyclerView.Adapter<*>?
    ) {
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
        recyclerView.adapter = adapter
    }

    @BindingAdapter("imageUrl")
    @JvmStatic
    fun bindRecyclerViewAdapter(imageView: ImageView, imageUrl: String?) {
        if (imageUrl != null) {
            // If we don't do this, you'll see the old image appear briefly
            // before it's replaced with the current image
            if (imageView.getTag(R.id.iv_thumb) == null || !imageView.getTag(R.id.iv_thumb)
                    .equals(imageUrl)
            ) {
                imageView.setImageBitmap(null)
                imageView.setTag(R.id.iv_thumb, imageUrl)
                Glide.with(imageView).load(imageUrl).into(imageView)
            }
        } else {
            imageView.setTag(R.id.iv_thumb, null)
            imageView.setImageBitmap(null)
        }
    }

}