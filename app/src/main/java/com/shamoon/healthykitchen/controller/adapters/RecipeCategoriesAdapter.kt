package com.shamoon.healthykitchen.controller.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.shamoon.healthykitchen.views.categories.CategoriesViewModel
import com.shamoon.healthykitchen.BR
import com.shamoon.healthykitchen.model.api.Category


class RecipeCategoriesAdapter : RecyclerView.Adapter<RecipeCategoriesAdapter.GenericViewHolder> {
    private var layoutId: Int
    private  var categories: ArrayList<Category> = mutableListOf<Category>() as ArrayList<Category>
    private  var viewModel: CategoriesViewModel

    constructor(
        @LayoutRes layoutId: Int,
        viewModel: CategoriesViewModel?
    ) {
        this.layoutId = layoutId
        this.viewModel = viewModel!!
    }

    private fun getLayoutIdForPosition(position: Int): Int {
        return layoutId
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericViewHolder {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            viewType,
            parent,
            false
        )

        return GenericViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return categories?.size ?: 0
    }

    override fun onBindViewHolder(holder: GenericViewHolder, position: Int) {
        holder.bind(viewModel, position)
    }

    override fun getItemViewType(position: Int): Int {
        return getLayoutIdForPosition(position)
    }

    fun setCategories(categories: ArrayList<Category>) {
        this.categories.clear()
        this.categories.addAll(categories)
    }

    class GenericViewHolder : ViewHolder {
        var binding: ViewDataBinding

        constructor(binding: ViewDataBinding) : super(binding.root) {
            this.binding = binding
        }

        fun bind(viewModel: CategoriesViewModel?, position: Int?) {

            binding.setVariable(BR.position, position)
            binding.setVariable(BR.viewModel, viewModel)
            binding.executePendingBindings()
        }

    }

}