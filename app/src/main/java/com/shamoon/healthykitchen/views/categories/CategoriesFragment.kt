package com.shamoon.healthykitchen.views.categories

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.shamoon.healthykitchen.R
import com.shamoon.healthykitchen.databinding.CategoriesFragmentBinding
import com.shamoon.healthykitchen.model.api.Category

class CategoriesFragment : Fragment() {

    companion object {
        fun newInstance() = CategoriesFragment()
    }

    private lateinit var viewModel: CategoriesViewModel
    private lateinit var binding: CategoriesFragmentBinding

    private lateinit var mListener: CategoriesListener

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.categories_fragment, container, false)
        activity!!.title = "Categories"
        return binding.getRoot()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupBindings(savedInstanceState)
    }

    private fun setupBindings(savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(this).get(CategoriesViewModel::class.java)

        if (savedInstanceState == null) {
            viewModel.init();
        }
        binding.setModel(viewModel);
        setupListUpdate();
    }

    private fun setupListUpdate() {
        viewModel.loading!!.set(View.VISIBLE)
        viewModel.fetchList()

        val observer = Observer<ArrayList<Category>> { categories ->
            // Update the UI, in this case, a TextView.
            viewModel.loading!!.set(View.GONE)
            if (categories.size == 0) {
                viewModel.showEmpty!!.set(View.VISIBLE)
            } else {
                viewModel.showEmpty!!.set(View.GONE)
                viewModel.setCategoriesInAdapter(categories)
            }
        }

        viewModel.getCategories().observe(viewLifecycleOwner, observer)

        setupListClick();
    }

    private fun setupListClick() {
        val observer = Observer<Category> { category ->
            if (category != null) {
                mListener.onCategoryClick(category.getStrCategory().toString())
            }
        }
        viewModel.selected.observe(viewLifecycleOwner, observer)
    }


    interface CategoriesListener {
        fun onCategoryClick(name: String)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is CategoriesListener) {
            mListener = context as CategoriesListener
        }
    }

}
