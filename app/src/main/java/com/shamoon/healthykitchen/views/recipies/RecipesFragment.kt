package com.shamoon.healthykitchen.views.recipies

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

import com.shamoon.healthykitchen.R
import com.shamoon.healthykitchen.databinding.RecipesFragmentBinding
import com.shamoon.healthykitchen.model.api.Meal

class RecipesFragment : Fragment() {

    companion object {
        fun newInstance() = RecipesFragment()
        @JvmStatic
        fun newInstance(recipeName: String) = RecipesFragment().apply {
            arguments = Bundle().apply {
                putString("recipeName", recipeName)
            }
        }
    }

    private lateinit var viewModel: RecipesViewModel
    private lateinit var mListener: RecipesListener
    // private var viewModel by ViewModel(RecipesViewModel)
    private lateinit var binding: RecipesFragmentBinding
    private lateinit var recipeName: String

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is RecipesListener) {
            mListener =  context as RecipesListener
        }

        arguments?.getString("recipeName")?.let {
            recipeName = it
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.recipes_fragment, container, false)
        activity!!.title = recipeName + " Recipes"
        return binding.getRoot()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupBindings(savedInstanceState)
    }

    private fun setupBindings(savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(this).get(RecipesViewModel::class.java)

        if (savedInstanceState == null) {
            viewModel.init();
        }
        //binding.recipesModel(viewModel);
        binding.recipesModel = viewModel
        setupListUpdate();
    }

    private fun setupListUpdate() {
        viewModel.loading!!.set(View.VISIBLE)
        viewModel.fetchList(recipeName)

        val observer = Observer<ArrayList<Meal>> { recipes ->
            // Update the UI, in this case, a TextView.
            viewModel.loading!!.set(View.GONE)
            if (recipes == null || recipes.size == 0) {
                viewModel.showEmpty!!.set(View.VISIBLE)
            } else {
                viewModel.showEmpty!!.set(View.GONE)
                viewModel.setRecipesInAdapter(recipes)
            }
        }

        viewModel.getRecipes().observe(viewLifecycleOwner, observer)

        setupListClick();
    }

    private fun setupListClick() {
        val observer = Observer<Meal> { recipe ->
            if (recipe != null) {
                mListener.openRecipe(recipe)
            }
        }
        viewModel.selected.observe(viewLifecycleOwner, observer)
    }

    interface RecipesListener{
        fun openRecipe(recipe: Meal)
    }

}
