package com.shamoon.healthykitchen.views.categories

import android.view.View
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shamoon.healthykitchen.R
import com.shamoon.healthykitchen.controller.adapters.RecipeCategoriesAdapter
import com.shamoon.healthykitchen.model.api.Category
import com.shamoon.healthykitchen.model.repositories.CategoriesRepository

class CategoriesViewModel : ViewModel() {
    lateinit var loading: ObservableInt
    lateinit var showEmpty: ObservableInt
    private lateinit var categoriesRepository: CategoriesRepository
    private lateinit var adapter: RecipeCategoriesAdapter

    lateinit var selected: MutableLiveData<Category>

    fun init() {
        categoriesRepository =
            CategoriesRepository()
        adapter = RecipeCategoriesAdapter(R.layout.category, this)
        loading = ObservableInt(View.GONE)
        showEmpty = ObservableInt(View.GONE)
        selected = MutableLiveData()
    }

    fun fetchList() {
        categoriesRepository!!.fetchList()
    }

    fun getCategories(): MutableLiveData<ArrayList<Category>> {
        return categoriesRepository.categories
    }

    fun getAdapter(): RecipeCategoriesAdapter? {
        return adapter
    }

    fun setCategoriesInAdapter(categories: ArrayList<Category>) {
        adapter!!.setCategories(categories)
        adapter!!.notifyDataSetChanged()
    }

    fun onItemClick(index: Int?) {
        val db: Category? = getCategoryAt(index)
        selected!!.setValue(db)
    }

    fun getCategoryAt(index: Int?): Category? {
        return if (categoriesRepository.categories
                .getValue() != null && index != null && categoriesRepository.categories
                .getValue()!!.size > index
        ) {
            categoriesRepository.categories.getValue()!!.get(index)
        } else null
    }
}
