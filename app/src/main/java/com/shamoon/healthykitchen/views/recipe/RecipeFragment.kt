package com.shamoon.healthykitchen.views.recipe

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener
import com.shamoon.healthykitchen.R
import com.shamoon.healthykitchen.databinding.RecipeFragmentBinding
import com.shamoon.healthykitchen.model.api.Meal


class RecipeFragment : Fragment(), YouTubePlayerListener,
    ViewModelStoreOwner {

    companion object {
        fun newInstance() =
            RecipeFragment()

        @JvmStatic
        fun newInstance(recipe: Meal) = RecipeFragment().apply {
            arguments = Bundle().apply {
                putSerializable("recipe", recipe)
            }
        }
    }

    private lateinit var viewModel: RecipeViewModel
    private lateinit var recipe: Meal
    private lateinit var binding: RecipeFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.recipe_fragment, container, false)
        activity!!.title = recipe.getStrMeal().toString()


        return binding.getRoot()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupBindings(savedInstanceState)
    }

    private fun setupBindings(savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(this).get(RecipeViewModel::class.java)

        if (savedInstanceState == null) {
            viewModel.init(recipe);
        } else {
            viewModel.setMeal(recipe)
        }
        binding.recipeModel = viewModel



        lifecycle.addObserver(binding.videoView)

        binding.videoView.addYouTubePlayerListener(this)


    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        arguments?.getSerializable("recipe")?.let {
            recipe = it as Meal
        }
    }

    override fun onApiChange(youTubePlayer: com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer) {
    }

    override fun onCurrentSecond(
        youTubePlayer: com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer,
        second: Float
    ) {
    }

    override fun onError(
        youTubePlayer: com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer,
        error: PlayerConstants.PlayerError
    ) {
    }

    override fun onPlaybackQualityChange(
        youTubePlayer: com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer,
        playbackQuality: PlayerConstants.PlaybackQuality
    ) {
    }

    override fun onPlaybackRateChange(
        youTubePlayer: com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer,
        playbackRate: PlayerConstants.PlaybackRate
    ) {
    }

    override fun onReady(player: com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer) {
        binding.videoViewThumbnail.visibility = View.GONE
        var videoId: String = recipe.getYoutubeId()
        player.cueVideo(videoId, 0f)
    }

    override fun onStateChange(
        youTubePlayer: com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer,
        state: PlayerConstants.PlayerState
    ) {
    }

    override fun onVideoDuration(
        youTubePlayer: com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer,
        duration: Float
    ) {
    }

    override fun onVideoId(
        youTubePlayer: com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer,
        videoId: String
    ) {
    }

    override fun onVideoLoadedFraction(
        youTubePlayer: com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer,
        loadedFraction: Float
    ) {
    }

}
