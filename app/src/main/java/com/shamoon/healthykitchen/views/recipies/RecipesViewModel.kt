package com.shamoon.healthykitchen.views.recipies

import android.view.View
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shamoon.healthykitchen.R
import com.shamoon.healthykitchen.controller.adapters.RecipesAdapter
import com.shamoon.healthykitchen.model.api.Meal
import com.shamoon.healthykitchen.model.repositories.RecipesRepository

class RecipesViewModel : ViewModel() {
    lateinit var loading: ObservableInt
    lateinit var showEmpty: ObservableInt
    private lateinit var recipesRepository: RecipesRepository
    private lateinit var adapter: RecipesAdapter

    lateinit var selected: MutableLiveData<Meal>

    fun init() {
        recipesRepository =
            RecipesRepository()
        adapter = RecipesAdapter(R.layout.recipes, this)
        loading = ObservableInt(View.GONE)
        showEmpty = ObservableInt(View.GONE)
        selected = MutableLiveData()
    }

    fun fetchList(name: String) {
        recipesRepository!!.fetchList(name)
    }

    fun getRecipes(): MutableLiveData<ArrayList<Meal>> {
        return recipesRepository.recipes
    }

    fun getAdapter(): RecipesAdapter? {
        return adapter
    }

    fun setRecipesInAdapter(recipes: ArrayList<Meal>) {
        adapter!!.setRecipes(recipes)
        adapter!!.notifyDataSetChanged()
    }

    fun onItemClick(index: Int?) {
        val db: Meal? = getRecipeAt(index)
        selected!!.setValue(db)
    }

    fun getRecipeAt(index: Int?): Meal? {
        return if (recipesRepository.recipes
                .getValue() != null && index != null && recipesRepository.recipes
                .getValue()!!.size > index
        ) {
            recipesRepository.recipes.getValue()!!.get(index)
        } else null
    }
}
