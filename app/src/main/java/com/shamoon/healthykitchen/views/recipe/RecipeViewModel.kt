package com.shamoon.healthykitchen.views.recipe

import androidx.lifecycle.ViewModel
import com.shamoon.healthykitchen.model.api.Meal

class RecipeViewModel : ViewModel() {
    private lateinit var recipe: Meal

    fun init(recipe: Meal){
        this.recipe = recipe
    }

    fun setMeal(recipe: Meal) {
        this.recipe = recipe
    }

    fun getMeal(): Meal? {
        return recipe
    }


}
