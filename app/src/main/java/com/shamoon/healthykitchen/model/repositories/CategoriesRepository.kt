package com.shamoon.healthykitchen.model.repositories

import android.util.Log
import androidx.databinding.BaseObservable
import androidx.lifecycle.MutableLiveData
import com.shamoon.healthykitchen.controller.service.Api
import com.shamoon.healthykitchen.model.api.Category
import com.shamoon.healthykitchen.model.api.RecipeCategoriesResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.Callback


class CategoriesRepository : BaseObservable() {
    private val categoryList: ArrayList<Category> = ArrayList()
    val categories: MutableLiveData<ArrayList<Category>> by lazy {
        MutableLiveData<ArrayList<Category>>()
    }


    fun fetchList() {
        val callback: Callback<RecipeCategoriesResponse?> =
            object : Callback<RecipeCategoriesResponse?> {
                override fun onFailure(call: Call<RecipeCategoriesResponse?>, t: Throwable) {
                    Log.e("RecipeCategory", t.message, t)
                }

                override fun onResponse(
                    call: Call<RecipeCategoriesResponse?>,
                    response: Response<RecipeCategoriesResponse?>
                ) {
                    if (response.isSuccessful){
                        val body: RecipeCategoriesResponse? = response.body()
                        categories.setValue(body!!.getCategories() as ArrayList<Category>?)
                        Log.d("shamoon", categories.toString())
                    }
                }

            }

        Api().getApi()!!.getRecipeCategories()!!.enqueue(callback)
    }

}