package com.shamoon.healthykitchen.model.api

import androidx.databinding.BaseObservable
import com.fasterxml.jackson.annotation.*
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)

class Category: BaseObservable() {

    @JsonProperty("idCategory")
    private var idCategory: String? = null

    @JsonProperty("strCategory")
    private var strCategory: String? = null

    @JsonProperty("strCategoryThumb")
    private var strCategoryThumb: String? = null

    @JsonProperty("strCategoryDescription")
    private var strCategoryDescription: String? = null

    @JsonIgnore
    private val additionalProperties: MutableMap<String, Any> =
        HashMap()

    @JsonProperty("idCategory")
    fun getIdCategory(): String? {
        return idCategory
    }

    @JsonProperty("idCategory")
    fun setIdCategory(idCategory: String?) {
        this.idCategory = idCategory
    }

    @JsonProperty("strCategory")
    fun getStrCategory(): String? {
        return strCategory
    }

    @JsonProperty("strCategory")
    fun setStrCategory(strCategory: String?) {
        this.strCategory = strCategory
    }

    @JsonProperty("strCategoryThumb")
    fun getStrCategoryThumb(): String? {
        return strCategoryThumb
    }

    @JsonProperty("strCategoryThumb")
    fun setStrCategoryThumb(strCategoryThumb: String?) {
        this.strCategoryThumb = strCategoryThumb
    }

    @JsonProperty("strCategoryDescription")
    fun getStrCategoryDescription(): String? {
        return strCategoryDescription
    }

    @JsonProperty("strCategoryDescription")
    fun setStrCategoryDescription(strCategoryDescription: String?) {
        this.strCategoryDescription = strCategoryDescription
    }

    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any>? {
        return additionalProperties
    }

    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        additionalProperties[name] = value
    }
}