package com.shamoon.healthykitchen.model.api

import com.fasterxml.jackson.annotation.*
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
class RecipeCategoriesResponse {
    @JsonProperty("categories")
    private var categories: List<Category?>? =
        null


    @JsonProperty("categories")
    public fun getCategories(): List<Category?>? {
        return categories
    }

    @JsonProperty("categories")
    public fun setCategories(categories: List<Category?>?) {
        this.categories = categories
    }
}