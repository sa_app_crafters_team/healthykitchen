package com.shamoon.healthykitchen.model.api

import com.fasterxml.jackson.annotation.*
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("meals")
class RecipesResponse {
    @JsonProperty("meals")
    private var meals: List<Meal?>? = null


    @JsonProperty("meals")
    fun getMeals(): List<Meal?>? {
        return meals
    }

    @JsonProperty("meals")
    fun setMeals(meals: List<Meal?>?) {
        this.meals = meals
    }
}