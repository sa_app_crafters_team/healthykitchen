package com.shamoon.healthykitchen.model.repositories

import android.util.Log
import androidx.databinding.BaseObservable
import androidx.lifecycle.MutableLiveData
import com.shamoon.healthykitchen.controller.service.Api
import com.shamoon.healthykitchen.model.api.Meal
import com.shamoon.healthykitchen.model.api.RecipesResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RecipesRepository : BaseObservable() {
    private val recipesList: ArrayList<Meal> = ArrayList()
    val recipes: MutableLiveData<ArrayList<Meal>> by lazy {
        MutableLiveData<ArrayList<Meal>>()
    }

    fun fetchList(name: String) {
        val callback: Callback<RecipesResponse?> =
            object : Callback<RecipesResponse?> {
                override fun onFailure(call: Call<RecipesResponse?>, t: Throwable) {
                    Log.e("RecipeCategory", t.message, t)
                }

                override fun onResponse(
                    call: Call<RecipesResponse?>,
                    response: Response<RecipesResponse?>
                ) {
                    if (response.isSuccessful) {
                        val body: RecipesResponse? = response.body()
                        recipes.setValue(body!!.getMeals() as ArrayList<Meal>?)
                    }
                }

            }

        Api().getApi()!!.search(name)!!.enqueue(callback)
    }
}