package com.shamoon.healthykitchen.model.api

import androidx.databinding.BaseObservable
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
class Meal : BaseObservable(), Serializable {
    @JsonProperty("idMeal")
    private var idMeal: Any? = null

    @JsonProperty("strMeal")
    private var strMeal: Any? = null

    @JsonProperty("strDrinkAlternate")
    private var strDrinkAlternate: Any? = null

    @JsonProperty("strCategory")
    private var strCategory: Any? = null

    @JsonProperty("strArea")
    private var strArea: Any? = null

    @JsonProperty("strInstructions")
    private var strInstructions: Any? = null

    @JsonProperty("strMealThumb")
    private var strMealThumb: Any? = null

    @JsonProperty("strTags")
    private var strTags: Any? = null

    @JsonProperty("strYoutube")
    private var strYoutube: Any? = null

    @JsonProperty("strIngredient1")
    private var strIngredient1: Any? = null

    @JsonProperty("strIngredient2")
    private var strIngredient2: Any? = null

    @JsonProperty("strIngredient3")
    private var strIngredient3: Any? = null

    @JsonProperty("strIngredient4")
    private var strIngredient4: Any? = null

    @JsonProperty("strIngredient5")
    private var strIngredient5: Any? = null

    @JsonProperty("strIngredient6")
    private var strIngredient6: Any? = null

    @JsonProperty("strIngredient7")
    private var strIngredient7: Any? = null

    @JsonProperty("strIngredient8")
    private var strIngredient8: Any? = null

    @JsonProperty("strIngredient9")
    private var strIngredient9: Any? = null

    @JsonProperty("strIngredient10")
    private var strIngredient10: Any? = null

    @JsonProperty("strIngredient11")
    private var strIngredient11: Any? = null

    @JsonProperty("strIngredient12")
    private var strIngredient12: Any? = null

    @JsonProperty("strIngredient13")
    private var strIngredient13: Any? = null

    @JsonProperty("strIngredient14")
    private var strIngredient14: Any? = null

    @JsonProperty("strIngredient15")
    private var strIngredient15: Any? = null

    @JsonProperty("strIngredient16")
    private var strIngredient16: Any? = null

    @JsonProperty("strIngredient17")
    private var strIngredient17: Any? = null

    @JsonProperty("strIngredient18")
    private var strIngredient18: Any? = null

    @JsonProperty("strIngredient19")
    private var strIngredient19: Any? = null

    @JsonProperty("strIngredient20")
    private var strIngredient20: Any? = null

    @JsonProperty("strMeasure1")
    private var strMeasure1: Any? = null

    @JsonProperty("strMeasure2")
    private var strMeasure2: Any? = null

    @JsonProperty("strMeasure3")
    private var strMeasure3: Any? = null

    @JsonProperty("strMeasure4")
    private var strMeasure4: Any? = null

    @JsonProperty("strMeasure5")
    private var strMeasure5: Any? = null

    @JsonProperty("strMeasure6")
    private var strMeasure6: Any? = null

    @JsonProperty("strMeasure7")
    private var strMeasure7: Any? = null

    @JsonProperty("strMeasure8")
    private var strMeasure8: Any? = null

    @JsonProperty("strMeasure9")
    private var strMeasure9: Any? = null

    @JsonProperty("strMeasure10")
    private var strMeasure10: Any? = null

    @JsonProperty("strMeasure11")
    private var strMeasure11: Any? = null

    @JsonProperty("strMeasure12")
    private var strMeasure12: Any? = null

    @JsonProperty("strMeasure13")
    private var strMeasure13: Any? = null

    @JsonProperty("strMeasure14")
    private var strMeasure14: Any? = null

    @JsonProperty("strMeasure15")
    private var strMeasure15: Any? = null

    @JsonProperty("strMeasure16")
    private var strMeasure16: Any? = null

    @JsonProperty("strMeasure17")
    private var strMeasure17: Any? = null

    @JsonProperty("strMeasure18")
    private var strMeasure18: Any? = null

    @JsonProperty("strMeasure19")
    private var strMeasure19: Any? = null

    @JsonProperty("strMeasure20")
    private var strMeasure20: Any? = null

    @JsonProperty("strSource")
    private var strSource: Any? = null

    @JsonProperty("dateModified")
    private var dateModified: Any? = null


    @JsonProperty("idMeal")
    fun getIdMeal(): Any? {
        return idMeal
    }

    @JsonProperty("idMeal")
    fun setIdMeal(idMeal: String?) {
        this.idMeal = idMeal
    }

    @JsonProperty("strMeal")
    fun getStrMeal(): Any? {
        return strMeal
    }

    @JsonProperty("strMeal")
    fun setStrMeal(strMeal: String?) {
        this.strMeal = strMeal
    }

    @JsonProperty("strDrinkAlternate")
    fun getStrDrinkAlternate(): Any? {
        return strDrinkAlternate
    }

    @JsonProperty("strDrinkAlternate")
    fun setStrDrinkAlternate(strDrinkAlternate: Any?) {
        this.strDrinkAlternate = strDrinkAlternate
    }

    @JsonProperty("strCategory")
    fun getStrCategory(): Any? {
        return strCategory
    }

    @JsonProperty("strCategory")
    fun setStrCategory(strCategory: Any?) {
        this.strCategory = strCategory
    }

    @JsonProperty("strArea")
    fun getStrArea(): Any? {
        return strArea
    }

    fun getCuisine(): String {
        if (strArea != null)
            return "Cuisine: " + strArea.toString()
        return ""
    }

    @JsonProperty("strArea")
    fun setStrArea(strArea: String?) {
        this.strArea = strArea
    }

    @JsonProperty("strInstructions")
    fun getStrInstructions(): Any? {
        return strInstructions
    }

    @JsonProperty("strInstructions")
    fun setStrInstructions(strInstructions: Any?) {
        this.strInstructions = strInstructions
    }

    @JsonProperty("strMealThumb")
    fun getStrMealThumb(): Any? {
        return strMealThumb
    }

    @JsonProperty("strMealThumb")
    fun setStrMealThumb(strMealThumb: Any?) {
        this.strMealThumb = strMealThumb
    }

    @JsonProperty("strTags")
    fun getStrTags(): Any? {
        return strTags
    }

    @JsonProperty("strTags")
    fun setStrTags(strTags: Any?) {
        this.strTags = strTags
    }

    @JsonProperty("strYoutube")
    fun getStrYoutube(): Any? {
        return strYoutube
    }

    fun getYoutubeId(): String{
        if (strYoutube != null){
            var parts = strYoutube.toString().split("v=")
            return parts[1]
        }else{
            return ""
        }
    }

    @JsonProperty("strYoutube")
    fun setStrYoutube(strYoutube: Any?) {
        this.strYoutube = strYoutube
    }

    @JsonProperty("strIngredient1")
    fun getStrIngredient1(): Any? {
        return strIngredient1
    }

    @JsonProperty("strIngredient1")
    fun setStrIngredient1(strIngredient1: Any?) {
        this.strIngredient1 = strIngredient1
    }

    @JsonProperty("strIngredient2")
    fun getStrIngredient2(): Any? {
        return strIngredient2
    }

    @JsonProperty("strIngredient2")
    fun setStrIngredient2(strIngredient2: Any?) {
        this.strIngredient2 = strIngredient2
    }

    @JsonProperty("strIngredient3")
    fun getStrIngredient3(): Any? {
        return strIngredient3
    }

    @JsonProperty("strIngredient3")
    fun setStrIngredient3(strIngredient3: Any?) {
        this.strIngredient3 = strIngredient3
    }

    @JsonProperty("strIngredient4")
    fun getStrIngredient4(): Any? {
        return strIngredient4
    }

    @JsonProperty("strIngredient4")
    fun setStrIngredient4(strIngredient4: Any?) {
        this.strIngredient4 = strIngredient4
    }

    @JsonProperty("strIngredient5")
    fun getStrIngredient5(): Any? {
        return strIngredient5
    }

    @JsonProperty("strIngredient5")
    fun setStrIngredient5(strIngredient5: Any?) {
        this.strIngredient5 = strIngredient5
    }

    @JsonProperty("strIngredient6")
    fun getStrIngredient6(): Any? {
        return strIngredient6
    }

    @JsonProperty("strIngredient6")
    fun setStrIngredient6(strIngredient6: Any?) {
        this.strIngredient6 = strIngredient6
    }

    @JsonProperty("strIngredient7")
    fun getStrIngredient7(): Any? {
        return strIngredient7
    }

    @JsonProperty("strIngredient7")
    fun setStrIngredient7(strIngredient7: Any?) {
        this.strIngredient7 = strIngredient7
    }

    @JsonProperty("strIngredient8")
    fun getStrIngredient8(): Any? {
        return strIngredient8
    }

    @JsonProperty("strIngredient8")
    fun setStrIngredient8(strIngredient8: Any?) {
        this.strIngredient8 = strIngredient8
    }

    @JsonProperty("strIngredient9")
    fun getStrIngredient9(): Any? {
        return strIngredient9
    }

    @JsonProperty("strIngredient9")
    fun setStrIngredient9(strIngredient9: Any?) {
        this.strIngredient9 = strIngredient9
    }

    @JsonProperty("strIngredient10")
    fun getStrIngredient10(): Any? {
        return strIngredient10
    }

    @JsonProperty("strIngredient10")
    fun setStrIngredient10(strIngredient10: Any?) {
        this.strIngredient10 = strIngredient10
    }

    @JsonProperty("strIngredient11")
    fun getStrIngredient11(): Any? {
        return strIngredient11
    }

    @JsonProperty("strIngredient11")
    fun setStrIngredient11(strIngredient11: Any?) {
        this.strIngredient11 = strIngredient11
    }

    @JsonProperty("strIngredient12")
    fun getStrIngredient12(): Any? {
        return strIngredient12
    }

    @JsonProperty("strIngredient12")
    fun setStrIngredient12(strIngredient12: Any?) {
        this.strIngredient12 = strIngredient12
    }

    @JsonProperty("strIngredient13")
    fun getStrIngredient13(): Any? {
        return strIngredient13
    }

    @JsonProperty("strIngredient13")
    fun setStrIngredient13(strIngredient13: Any?) {
        this.strIngredient13 = strIngredient13
    }

    @JsonProperty("strIngredient14")
    fun getStrIngredient14(): Any? {
        return strIngredient14
    }

    @JsonProperty("strIngredient14")
    fun setStrIngredient14(strIngredient14: Any?) {
        this.strIngredient14 = strIngredient14
    }

    @JsonProperty("strIngredient15")
    fun getStrIngredient15(): Any? {
        return strIngredient15
    }

    @JsonProperty("strIngredient15")
    fun setStrIngredient15(strIngredient15: Any?) {
        this.strIngredient15 = strIngredient15
    }

    @JsonProperty("strIngredient16")
    fun getStrIngredient16(): Any? {
        return strIngredient16
    }

    @JsonProperty("strIngredient16")
    fun setStrIngredient16(strIngredient16: Any?) {
        this.strIngredient16 = strIngredient16
    }

    @JsonProperty("strIngredient17")
    fun getStrIngredient17(): Any? {
        return strIngredient17
    }

    @JsonProperty("strIngredient17")
    fun setStrIngredient17(strIngredient17: Any?) {
        this.strIngredient17 = strIngredient17
    }

    @JsonProperty("strIngredient18")
    fun getStrIngredient18(): Any? {
        return strIngredient18
    }

    @JsonProperty("strIngredient18")
    fun setStrIngredient18(strIngredient18: Any?) {
        this.strIngredient18 = strIngredient18
    }

    @JsonProperty("strIngredient19")
    fun getStrIngredient19(): Any? {
        return strIngredient19
    }

    @JsonProperty("strIngredient19")
    fun setStrIngredient19(strIngredient19: Any?) {
        this.strIngredient19 = strIngredient19
    }

    @JsonProperty("strIngredient20")
    fun getStrIngredient20(): Any? {
        return strIngredient20
    }

    @JsonProperty("strIngredient20")
    fun setStrIngredient20(strIngredient20: Any?) {
        this.strIngredient20 = strIngredient20
    }

    fun Any?.notNull(f: ()-> Unit){
        if (this != null){
            f()
        }
    }

    fun getIngredients(): String {
        var result: String = ""

        strIngredient1.notNull { result += strIngredient1 }
        strIngredient2.notNull { result += System.lineSeparator() + strIngredient2 }
        strIngredient3.notNull { result += System.lineSeparator() + strIngredient3 }
        strIngredient4.notNull { result += System.lineSeparator() + strIngredient4 }
        strIngredient5.notNull { result += System.lineSeparator() + strIngredient5 }
        strIngredient6.notNull { result += System.lineSeparator() + strIngredient6 }
        strIngredient7.notNull { result += System.lineSeparator() + strIngredient7 }
        strIngredient8.notNull { result += System.lineSeparator() + strIngredient8 }
        strIngredient9.notNull { result += System.lineSeparator() + strIngredient9 }
        strIngredient10.notNull { result += System.lineSeparator() + strIngredient10 }
        strIngredient11.notNull { result += System.lineSeparator() + strIngredient11 }
        strIngredient12.notNull { result += System.lineSeparator() + strIngredient12 }
        strIngredient13.notNull { result += System.lineSeparator() + strIngredient13 }
        strIngredient14.notNull { result += System.lineSeparator() + strIngredient14 }
        strIngredient15.notNull { result += System.lineSeparator() + strIngredient15 }
        strIngredient16.notNull { result += System.lineSeparator() + strIngredient16 }
        strIngredient17.notNull { result += System.lineSeparator() + strIngredient17 }
        strIngredient18.notNull { result += System.lineSeparator() + strIngredient18 }
        strIngredient19.notNull { result += System.lineSeparator() + strIngredient19 }
        strIngredient20.notNull { result += System.lineSeparator() + strIngredient20 }

        return result
    }

    @JsonProperty("strMeasure1")
    fun getStrMeasure1(): Any? {
        return strMeasure1
    }

    @JsonProperty("strMeasure1")
    fun setStrMeasure1(strMeasure1: Any?) {
        this.strMeasure1 = strMeasure1
    }

    @JsonProperty("strMeasure2")
    fun getStrMeasure2(): Any? {
        return strMeasure2
    }

    @JsonProperty("strMeasure2")
    fun setStrMeasure2(strMeasure2: Any?) {
        this.strMeasure2 = strMeasure2
    }

    @JsonProperty("strMeasure3")
    fun getStrMeasure3(): Any? {
        return strMeasure3
    }

    @JsonProperty("strMeasure3")
    fun setStrMeasure3(strMeasure3: Any?) {
        this.strMeasure3 = strMeasure3
    }

    @JsonProperty("strMeasure4")
    fun getStrMeasure4(): Any? {
        return strMeasure4
    }

    @JsonProperty("strMeasure4")
    fun setStrMeasure4(strMeasure4: Any?) {
        this.strMeasure4 = strMeasure4
    }

    @JsonProperty("strMeasure5")
    fun getStrMeasure5(): Any? {
        return strMeasure5
    }

    @JsonProperty("strMeasure5")
    fun setStrMeasure5(strMeasure5: String?) {
        this.strMeasure5 = strMeasure5
    }

    @JsonProperty("strMeasure6")
    fun getStrMeasure6(): Any? {
        return strMeasure6
    }

    @JsonProperty("strMeasure6")
    fun setStrMeasure6(strMeasure6: Any?) {
        this.strMeasure6 = strMeasure6
    }

    @JsonProperty("strMeasure7")
    fun getStrMeasure7(): Any? {
        return strMeasure7
    }

    @JsonProperty("strMeasure7")
    fun setStrMeasure7(strMeasure7: Any?) {
        this.strMeasure7 = strMeasure7
    }

    @JsonProperty("strMeasure8")
    fun getStrMeasure8(): Any? {
        return strMeasure8
    }

    @JsonProperty("strMeasure8")
    fun setStrMeasure8(strMeasure8: Any?) {
        this.strMeasure8 = strMeasure8
    }

    @JsonProperty("strMeasure9")
    fun getStrMeasure9(): Any? {
        return strMeasure9
    }

    @JsonProperty("strMeasure9")
    fun setStrMeasure9(strMeasure9: Any?) {
        this.strMeasure9 = strMeasure9
    }

    @JsonProperty("strMeasure10")
    fun getStrMeasure10(): Any? {
        return strMeasure10
    }

    @JsonProperty("strMeasure10")
    fun setStrMeasure10(strMeasure10: Any?) {
        this.strMeasure10 = strMeasure10
    }

    @JsonProperty("strMeasure11")
    fun getStrMeasure11(): Any? {
        return strMeasure11
    }

    @JsonProperty("strMeasure11")
    fun setStrMeasure11(strMeasure11: Any?) {
        this.strMeasure11 = strMeasure11
    }

    @JsonProperty("strMeasure12")
    fun getStrMeasure12(): Any? {
        return strMeasure12
    }

    @JsonProperty("strMeasure12")
    fun setStrMeasure12(strMeasure12: Any?) {
        this.strMeasure12 = strMeasure12
    }

    @JsonProperty("strMeasure13")
    fun getStrMeasure13(): Any? {
        return strMeasure13
    }

    @JsonProperty("strMeasure13")
    fun setStrMeasure13(strMeasure13: Any?) {
        this.strMeasure13 = strMeasure13
    }

    @JsonProperty("strMeasure14")
    fun getStrMeasure14(): Any? {
        return strMeasure14
    }

    @JsonProperty("strMeasure14")
    fun setStrMeasure14(strMeasure14: Any?) {
        this.strMeasure14 = strMeasure14
    }

    @JsonProperty("strMeasure15")
    fun getStrMeasure15(): Any? {
        return strMeasure15
    }

    @JsonProperty("strMeasure15")
    fun setStrMeasure15(strMeasure15: Any?) {
        this.strMeasure15 = strMeasure15
    }

    @JsonProperty("strMeasure16")
    fun getStrMeasure16(): Any? {
        return strMeasure16
    }

    @JsonProperty("strMeasure16")
    fun setStrMeasure16(strMeasure16: Any?) {
        this.strMeasure16 = strMeasure16
    }

    @JsonProperty("strMeasure17")
    fun getStrMeasure17(): Any? {
        return strMeasure17
    }

    @JsonProperty("strMeasure17")
    fun setStrMeasure17(strMeasure17: Any?) {
        this.strMeasure17 = strMeasure17
    }

    @JsonProperty("strMeasure18")
    fun getStrMeasure18(): Any? {
        return strMeasure18
    }

    @JsonProperty("strMeasure18")
    fun setStrMeasure18(strMeasure18: Any?) {
        this.strMeasure18 = strMeasure18
    }

    @JsonProperty("strMeasure19")
    fun getStrMeasure19(): Any? {
        return strMeasure19
    }

    @JsonProperty("strMeasure19")
    fun setStrMeasure19(strMeasure19: Any?) {
        this.strMeasure19 = strMeasure19
    }

    @JsonProperty("strMeasure20")
    fun getStrMeasure20(): Any? {
        return strMeasure20
    }

    @JsonProperty("strMeasure20")
    fun setStrMeasure20(strMeasure20: Any?) {
        this.strMeasure20 = strMeasure20
    }

    @JsonProperty("strSource")
    fun getStrSource(): Any? {
        return strSource
    }

    fun getSource(): String {
        if (strSource != null)
            return "Source: " + strSource
        else
            return ""
    }

    @JsonProperty("strSource")
    fun setStrSource(strSource: Any?) {
        this.strSource = strSource
    }

    @JsonProperty("dateModified")
    fun getDateModified(): Any? {
        return dateModified
    }

    @JsonProperty("dateModified")
    fun setDateModified(dateModified: Any?) {
        this.dateModified = dateModified
    }
}