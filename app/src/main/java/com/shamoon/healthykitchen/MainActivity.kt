package com.shamoon.healthykitchen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.shamoon.healthykitchen.model.api.Meal
import com.shamoon.healthykitchen.views.categories.CategoriesFragment
import com.shamoon.healthykitchen.views.recipe.RecipeFragment
import com.shamoon.healthykitchen.views.recipies.RecipesFragment

class MainActivity : AppCompatActivity(), CategoriesFragment.CategoriesListener, RecipesFragment.RecipesListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, CategoriesFragment.newInstance(), CategoriesFragment::class.java.simpleName)
                    .commitNow()
        }
    }

    override fun onCategoryClick(name: String) {
        //from here I will go to next list of recipes
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, RecipesFragment.newInstance(name))
            .addToBackStack(RecipesFragment::class.java.simpleName)
            .commit()
    }

    override fun openRecipe(recipe: Meal) {
        //from here I will go show individual recipe
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, RecipeFragment.newInstance(recipe))
            .addToBackStack(RecipeFragment::class.java.simpleName)
            .commit()
    }
}
