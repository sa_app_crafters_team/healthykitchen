# README #

This README would normally document whatever steps are necessary to get your application up and running.

Introduction
This application is written in Kotlin, in applicaiton using TheMealDB API (https://themealdb.com/api.php) user can see 
recipe categories and their thumbnails in a list. Selecting a category will show you a list of recipes for that category 
and then you will see each recipe individually on selecting any recipe.

Application version is 1.0.0


### How do I get set up? ###

run following command in your terminal or command prompt:
git clone https://bitbucket.org/shamoon192/healthykitchen.git

Summary of set up: you need to have most updated version of android studio with updated android SDK with all tools installed. Then open project in your android studio and wait for a build, once you got successful build then connect android device with minimum sdk version 19 and run application using adb of android studio.
* Configuration
Dependencies: databinding, glide, lifecycle-extensions, lifecycle-viewmodel-ktx, legacy-support-v4, junit, ext:junit, espresso-core, recyclerview, cardview, retrofit2, converter-jackson, constraintlayout, kotlin, appcompat, core-ktx

* How to run tests
I have written two unit test cases to test api is working fine and one android UI test to check that it
should passed to open any category. following are two test classes:
1- com.shamoon.healthykitchen.ApiUnitTest
2- com.shamoon.healthykitchen.MainActivityTest
to run any test you need to right click on test class and run from options given

### Contribution guidelines: Kindly don't try to commit or push code to any branch. Its for task purpose ###


### Who do I talk to? ###

Repo owner or admin: Shamoon Ahmed 
LinkedIn: https://www.linkedin.com/in/shamoon-software-developer
WebSite: https://shamoonahmed1992.wixsite.com/shamooncv